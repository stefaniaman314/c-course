#include <iostream>

void decimalToBinary(unsigned number)
{
	const auto nBits = 32; //sizeof(uint) = 4

	for (auto index = nBits - 1; index >= 0; --index)
	{
		auto result = number >> index;

		if (result & 1)
			std::cout << "1";
		else
			std::cout << "0";
	}
}

unsigned binaryToDecimal(unsigned number)
{
	unsigned result = 0;
	auto index = 0;

	while (number)
	{
		auto digit = number % 10;
		result += digit << index;
		number /= 10;
		++index;
	}

	return result;
}

int main()
{
	std::cout << "Size of UInt:" << sizeof(unsigned);

	unsigned number;
	std::cout << "\nEnter number in decimal: ";
	std::cin >> number;

	std::cout << "Decimal -> Binary: ";
	decimalToBinary(number);

	std::cout << "\nEnter number in binary: ";
	std::cin >> number;

	std::cout << "Binary -> Decimal: " << binaryToDecimal(number) << '\n';

	return 0;
}