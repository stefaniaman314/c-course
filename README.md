# C++ Course

## Workshop 1:
- Introduction, evaluation & expectations
- Program structure
- Language elements
- Data types & literals
- Operators & namespaces
- Input & output
- Live Code: binary converter
### Homework: use bitwise operations to do the conversions.

## Workshop 2:
- Arrays, char arrays & STD arrays.
- Statements: selection, iteration, jump.
- Functions and their parameters (default parameters).
- Data structures: struct, union, enum & user defined data types (typedef and class).
- OOP in C++ & the diamond problem.
- Live Code: Inheritance examples, casting, virtual allocation table.
### Homework: Polimorfism example using geometric shapes hierarchy.

## Workshop 3:
- Heap vs Stack.
- Memory: static vs dynamic.
- Smart pointers: unique vs shared.
- Threading.
- Live Code: Threading and memory management examples.
### Homework: Function that make conversions from any base to any base. Threaded example of converting 5 million numbers from any base into other bases in less than 2 seconds

### Workshop 4:
- New projects & project architecture.
- API & dependencies.
- Static libraries.
- Dynamic libraries.
- Live Code: examples of *.lib and *.dll, some SDL.
### Homework: build an SDL game like X&O or snake with a fully functioning GUI.

